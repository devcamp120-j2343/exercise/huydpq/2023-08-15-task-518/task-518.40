import styled from "styled-components";

export default styled.button({
    backgroundColor:"#7B4CD8",
    color:"white",
    border:"none",
    borderRadius:"5px",
    padding:"10px",
    margin:"10px"
})

/*    
export default styled['button']`
    background-color:#7B4CD8;
    color:white;
    border:none;
    border-radius:5px;
    padding:10px;
    margin:10px;
    `  */  
/*
export default styled.button`
    background-color:#7B4CD8;
    color:white;
    border:none;
    border-radius:5px;
    padding:10px;
    margin:10px;
    `   */ 