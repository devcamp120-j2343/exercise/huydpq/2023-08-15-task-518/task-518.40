import { SuperButton, ChildButton } from "./components/ExtendButton";
import {PropButton, PrimaryButton} from "./components/PropButton";
import StyledButton from "./components/StyledButton";

function App() {
  return (
    <div>
      <div>
        <StyledButton>I am a styled button!</StyledButton>
      </div>
      <div>
        <PropButton bgColor="#C75E6B" fontColor="#C03932">Button 1</PropButton>
        <PropButton bgColor="#FFDACA" fontColor="#DE825B">Button 2</PropButton>
        <PropButton bgColor="#FFF4C7" fontColor="#E3C25C">Button 3</PropButton>
        <PropButton>Button 4</PropButton>
      </div>
      <div>
        <PrimaryButton primary>Primary Button</PrimaryButton>
        <PrimaryButton>Other Button 1</PrimaryButton>
        <PrimaryButton>Other Button 2</PrimaryButton>
      </div>
      <div>
        <SuperButton>Super Button</SuperButton>
        <ChildButton>Hello 1</ChildButton> 
        <ChildButton bgColor="#FF1993" fontColor="#ffffff">Hello 2</ChildButton>
        <ChildButton bgColor="#CBB057" fontColor="#fefefe">Hello 3</ChildButton>
      </div>
    </div>
  );
}

export default App;
